package seis720.project.entity;

import seis720.project.algorithms.DES.DESType;

/**
 * @author Sabin Gautam
 *
 */
public class CBCDESPerformanceStats extends DESPerformanceStats {

    public CBCDESPerformanceStats() {
        super(DESType.DES_CBC);
    }

    public CBCDESPerformanceStats(DESType desType) {
        super(DESType.DES_CBC);
    }
}
