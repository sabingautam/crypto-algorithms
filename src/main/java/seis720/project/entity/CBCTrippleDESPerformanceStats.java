package seis720.project.entity;

import seis720.project.algorithms.AES.AESType;
import seis720.project.algorithms.TrippleDES.TrippleDESType;

/**
 * @author Sabin Gautam
 *
 */
public class CBCTrippleDESPerformanceStats extends TrippleDESPerformanceStats {

    public CBCTrippleDESPerformanceStats() {
        super(TrippleDESType.DES_3_CBC);
    }

    public CBCTrippleDESPerformanceStats(AESType aesType) {
        super(TrippleDESType.DES_3_CBC);
    }
}
