package seis720.project.entity;

import seis720.project.algorithms.AES.AESType;

/**
 * @author Sabin Gautam
 *
 */
public class AESPerformanceStats extends PerformanceStats {

    private AESType aesType;

    public AESPerformanceStats (AESType aesType) {
        super();
        this.aesType = aesType;
    }

    public AESType getAesType() {
        return this.aesType;
    }
}
