package seis720.project.entity;

import seis720.project.algorithms.TrippleDES.TrippleDESType;

/**
 * @author Sabin Gautam
 *
 */
public class TrippleDESPerformanceStats extends PerformanceStats {

    private TrippleDESType trippleDESType;

    public TrippleDESPerformanceStats (TrippleDESType trippleDESType) {
        super();
        this.trippleDESType = trippleDESType;
    }

    public TrippleDESType getTrippleDesType() {
        return this.trippleDESType;
    }
}
