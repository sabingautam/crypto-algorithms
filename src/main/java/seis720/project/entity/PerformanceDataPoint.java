package seis720.project.entity;

/**
 * 
 * @author Sabin Gautam
 *
 */
public class PerformanceDataPoint {

    private Long encryptionTime = Long.MIN_VALUE;
    private Long decryptionTime = Long.MIN_VALUE;
    private Long keyGenerationTimeTaken = Long.MIN_VALUE;
    private int blockSize = 0;
    private int keySize = 0;
    private int textSize = 0;

    public PerformanceDataPoint() {
        super();
    }

    public PerformanceDataPoint(Long encryptionTime,
                                Long decryptionTime,
                                Long keyGenerationTimeTaken,
                                int blockSize,
                                int keySize,
                                int textSize) {
        this.encryptionTime = encryptionTime;
        this.decryptionTime = decryptionTime;
        this.keyGenerationTimeTaken = keyGenerationTimeTaken;
        this.blockSize = blockSize;
        this.keySize = keySize;
        this.textSize = textSize;
    }

    public Long getEncryptionTime() {
        return encryptionTime;
    }

    public void setEncryptionTime(Long encryptionTime) {
        this.encryptionTime = encryptionTime;
    }

    public Long getDecryptionTime() {
        return decryptionTime;
    }

    public void setDecryptionTime(Long decryptionTime) {
        this.decryptionTime = decryptionTime;
    }

    public int getBlockSize() {
        return blockSize;
    }

    public void setBlockSize(int blockSize) {
        this.blockSize = blockSize;
    }

    public int getKeySize() {
        return keySize;
    }

    public void setKeySize(int keySize) {
        this.keySize = keySize;
    }

    public int getTextSize() {
        return textSize;
    }

    public void setTextSize(int textSize) {
        this.textSize = textSize;
    }

    public Long getKeyGenerationTimeTaken() {
        return this.keyGenerationTimeTaken;
    }

    public void setKeyGenerationTimeTaken(Long keyGenerationTimeTaken) {
        this.keyGenerationTimeTaken = keyGenerationTimeTaken;
    }
}
