package seis720.project.entity;

import seis720.project.algorithms.RSA.RSAType;

/**
 * @author Sabin Gautam
 *
 */
public class RSAPerformanceStats extends PerformanceStats {
    
    private RSAType rsaType; 

    public RSAPerformanceStats (RSAType rsaType) {
        super();
        this.rsaType = rsaType;
    }

    public RSAType getRsaType() {
        return this.rsaType;
    }
}
