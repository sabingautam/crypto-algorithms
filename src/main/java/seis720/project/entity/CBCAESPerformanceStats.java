package seis720.project.entity;

import seis720.project.algorithms.AES.AESType;

/**
 * 
 * @author Sabin Gautam
 *
 */
public class CBCAESPerformanceStats extends AESPerformanceStats {

    public CBCAESPerformanceStats() {
        super(AESType.AES_CBC);
    }

    public CBCAESPerformanceStats(AESType aesType) {
        super(AESType.AES_CBC);
    }

}
