package seis720.project.entity;

import seis720.project.algorithms.AES.AESType;

/**
 * 
 * @author Sabin Gautam
 *
 */
public class ECBAESPerformanceStats extends AESPerformanceStats {

    public ECBAESPerformanceStats() {
        super(AESType.AES_ECB);
    }

    public ECBAESPerformanceStats(AESType aesType) {
        super(AESType.AES_ECB);
    }

}