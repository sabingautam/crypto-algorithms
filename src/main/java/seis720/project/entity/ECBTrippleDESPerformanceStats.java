package seis720.project.entity;

import seis720.project.algorithms.AES.AESType;
import seis720.project.algorithms.TrippleDES.TrippleDESType;

/**
 * @author Sabin Gautam
 *
 */
public class ECBTrippleDESPerformanceStats extends TrippleDESPerformanceStats {

    public ECBTrippleDESPerformanceStats() {
        super(TrippleDESType.DES_3_ECB);
    }

    public ECBTrippleDESPerformanceStats(AESType aesType) {
        super(TrippleDESType.DES_3_ECB);
    }
}
