package seis720.project.entity;

import seis720.project.algorithms.DES.DESType;

/**
 * 
 * @author  Sabin Gautam
 *
 */
public class ECBDESPerformanceStats extends DESPerformanceStats {

    public ECBDESPerformanceStats() {
        super(DESType.DES_CBC);
    }

    public ECBDESPerformanceStats(DESType desType) {
        super(DESType.DES_CBC);
    }
}
