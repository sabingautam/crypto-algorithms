package seis720.project.entity;

import java.util.List;

/**
 * @author Sabin Gautam
 *
 */
public interface IPerformanceStats {

    void addPerformanceDataPoint(PerformanceDataPoint dataPoint);

    List<PerformanceDataPoint> getAllPerformanceDataPoint();

}
