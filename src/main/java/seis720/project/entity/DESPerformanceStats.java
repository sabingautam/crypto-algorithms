package seis720.project.entity;

import seis720.project.algorithms.DES.DESType;

/**
 * @author Sabin Gautam
 *
 */
public class DESPerformanceStats extends PerformanceStats {

    private DESType desType;

    public DESPerformanceStats (DESType desType) {
        super();
        this.desType = desType;
    }

    public DESType getDesType() {
        return this.desType;
    }
}
