package seis720.project.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Sabin Gautam
 *
 */
public abstract class PerformanceStats implements IPerformanceStats {

    private List<PerformanceDataPoint> performanceStats = new ArrayList<PerformanceDataPoint>();

    public void addPerformanceDataPoint(PerformanceDataPoint dataPoint) {
        this.performanceStats.add(dataPoint);
    }

    public List<PerformanceDataPoint> getAllPerformanceDataPoint() {
        return performanceStats;
    }
}
