package seis720.project.main;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.crypto.NoSuchPaddingException;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import seis720.project.algorithms.AES;
import seis720.project.algorithms.AES.AESType;
import seis720.project.algorithms.DES;
import seis720.project.algorithms.DES.DESType;
import seis720.project.algorithms.IAlgorithm;
import seis720.project.algorithms.RSA;
import seis720.project.algorithms.RSA.RSAType;
import seis720.project.algorithms.TrippleDES;
import seis720.project.algorithms.TrippleDES.TrippleDESType;
import seis720.project.entity.IPerformanceStats;
import seis720.project.entity.PerformanceDataPoint;
import seis720.project.service.PerformanceRecordingService;

@Component
public class CryptoAlgorithmsPerfomanceTester {

    @Autowired
    PerformanceRecordingService recordingService;

    @Value("${cbc.des.plain.text}")
    private String cbcDESPlainText;

    @Value("${ecb.des.plain.text}")
    private String ecbDESPlainText;

    @Value("${cbc.tripple.des.plain.text}")
    private String cbcTrippleDESPlainText;

    @Value("${ecb.tripple.des.plain.text}")
    private String ecbTrippleDESPlainText;

    @Value("${cbc.aes.plain.text}")
    private String cbcAESPlainText;

    @Value("${ecb.aes.plain.text}")
    private String ecbAESPlainText;

    @Value("${rsa.plain.text}")
    private String rsaPlainText;

    @SuppressWarnings("resource")
    public static void main(String[] args) throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("/applicationContext.xml");
        CryptoAlgorithmsPerfomanceTester cryptoTester = context.getBean(CryptoAlgorithmsPerfomanceTester.class);
        cryptoTester.runDESPerformanceTest();
        cryptoTester.runTrippleDESPerformanceTest();
        cryptoTester.runAESPerformanceTest();
//        cryptoTester.runRSAPerformanceTest();
        cryptoTester.generateReport();
    }

    
    private void runDESPerformanceTest() throws NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException, NoSuchProviderException {
        runDESECBTest();
        runDESCBCTest();
    }

    private void runDESECBTest() throws NoSuchAlgorithmException, NoSuchPaddingException, NoSuchProviderException {
        IAlgorithm ecbDES = new DES(recordingService, DESType.DES_ECB);
        runCryptoTest(ecbDESPlainText, ecbDES);
    }

    private void runDESCBCTest() throws NoSuchAlgorithmException, NoSuchPaddingException, NoSuchProviderException {
        IAlgorithm cbcDES = new DES(recordingService, DESType.DES_CBC);
        runCryptoTest(cbcDESPlainText, cbcDES);
    }

    private void runTrippleDESPerformanceTest() throws NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException, NoSuchProviderException {
        runTrippleDESECBTest();
        runTrippleDESCBCTest();
    }

    private void runTrippleDESCBCTest() throws NoSuchAlgorithmException, NoSuchPaddingException, NoSuchProviderException {
        IAlgorithm cbcTrippleDES = new TrippleDES(recordingService, TrippleDESType.DES_3_CBC);
        runCryptoTest(cbcTrippleDESPlainText, cbcTrippleDES);
    }

    private void runTrippleDESECBTest() throws NoSuchAlgorithmException, NoSuchPaddingException, NoSuchProviderException {
        IAlgorithm ecbTrippleDES = new TrippleDES(recordingService, TrippleDESType.DES_3_ECB);
        runCryptoTest(ecbTrippleDESPlainText, ecbTrippleDES);
    }

    private void runAESPerformanceTest() throws NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException, NoSuchProviderException {
        runAESECBTest();
        runAESCBCTest();
    }

    private void runAESECBTest() throws NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException, NoSuchProviderException {
        IAlgorithm ecbAES = new AES(recordingService, AESType.AES_ECB);
        runCryptoTest(ecbAESPlainText, ecbAES);
    }

    private void runAESCBCTest() throws NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException, NoSuchProviderException {
        AES cbcAES = new AES(recordingService, AESType.AES_CBC);
        runCryptoTest(cbcAESPlainText, cbcAES);
    }

    private void runRSAPerformanceTest() throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, SignatureException, InvalidKeySpecException, NoSuchProviderException {
        RSA rsa = new RSA(recordingService, RSAType.RSA_ECB);
        runCryptoTest(rsaPlainText, rsa);
    }

    private void runCryptoTest(String plainText, IAlgorithm algorithm) throws NoSuchAlgorithmException, NoSuchPaddingException, NoSuchProviderException {
        //String addCharacters = plainText;
        for (int i=0; i<1000; i++) {
            algorithm.decrypt(algorithm.encrypt(plainText));
            algorithm.recordPerformanceDataPoint();
            plainText = plainText + i;
            if (plainText.getBytes().length > 335609851) {
                break;
            }
        }
    }

    private void generateReport() {
        try {
            Map<String, IPerformanceStats> performanceStatsList = recordingService.getAllPerformanceStats();
            for (String cryptoAlgorithmName : performanceStatsList.keySet()) {

                writeEncryptionAndDecryptionTimes(performanceStatsList, cryptoAlgorithmName);
                writeTextSizeEncryptionTimeCsv(performanceStatsList, cryptoAlgorithmName);
                writeTextSizeDecryptionTimeCsv(performanceStatsList, cryptoAlgorithmName);
                writeKeySizeEncryptionTimeCsv(performanceStatsList, cryptoAlgorithmName);
                writeKeySizeDecryptionTimeCsv(performanceStatsList, cryptoAlgorithmName);
                writeKeyGenerationTimeCsv(performanceStatsList, cryptoAlgorithmName);

                System.out.println("Performance test done for - " + cryptoAlgorithmName);
            }
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }


    private void writeTextSizeEncryptionTimeCsv(
            Map<String, IPerformanceStats> performanceStatsList, String cryptoAlgorithmName)
            throws IOException {
        String fileName = "textSizeEncryption" + cryptoAlgorithmName + ".csv";
        FileUtils.touch(new File(fileName));
        FileUtils.forceDelete(new File(fileName));

        String csvHeader = String.format("%s,%s", "text size", "encryption time");
        Collection<String> csvData = new ArrayList<String>();
        csvData.add(csvHeader);

        List<PerformanceDataPoint> dataPoints = performanceStatsList.get(cryptoAlgorithmName).getAllPerformanceDataPoint();
        for (PerformanceDataPoint dataPoint : dataPoints) {
            String csvDataLines = String.format("%s,%S", dataPoint.getTextSize(), dataPoint.getEncryptionTime());
            csvData.add(csvDataLines);
        }
        FileUtils.writeLines(new File(fileName), csvData);
    }

    private void writeTextSizeDecryptionTimeCsv(
            Map<String, IPerformanceStats> performanceStatsList, String cryptoAlgorithmName)
            throws IOException {
        String fileName = "textSizeDecryption" + cryptoAlgorithmName + ".csv";
        FileUtils.touch(new File(fileName));
        FileUtils.forceDelete(new File(fileName));

        String csvHeader = String.format("%s,%s", "text size", "decryption time");
        Collection<String> csvData = new ArrayList<String>();
        csvData.add(csvHeader);

        List<PerformanceDataPoint> dataPoints = performanceStatsList.get(cryptoAlgorithmName).getAllPerformanceDataPoint();
        for (PerformanceDataPoint dataPoint : dataPoints) {
            String csvDataLines = String.format("%s,%S", dataPoint.getTextSize(), dataPoint.getDecryptionTime());
            csvData.add(csvDataLines);
        }
        FileUtils.writeLines(new File(fileName), csvData);
    }

    private void writeKeySizeEncryptionTimeCsv(
            Map<String, IPerformanceStats> performanceStatsList, String cryptoAlgorithmName)
            throws IOException {
        String fileName = "keySizeEncryptionTime" + cryptoAlgorithmName + ".csv";
        FileUtils.touch(new File(fileName));
        FileUtils.forceDelete(new File(fileName));

        String csvHeader = String.format("%s,%s", "key size", "encryption time");
        Collection<String> csvData = new ArrayList<String>();
        csvData.add(csvHeader);

        List<PerformanceDataPoint> dataPoints = performanceStatsList.get(cryptoAlgorithmName).getAllPerformanceDataPoint();
        for (PerformanceDataPoint dataPoint : dataPoints) {
            String csvDataLines = String.format("%s,%S", dataPoint.getKeySize(), dataPoint.getEncryptionTime());
            csvData.add(csvDataLines);
        }
        FileUtils.writeLines(new File(fileName), csvData);
    }
    
    private void writeKeySizeDecryptionTimeCsv(
            Map<String, IPerformanceStats> performanceStatsList, String cryptoAlgorithmName)
            throws IOException {
        String fileName = "keySizeDecryptionTime" + cryptoAlgorithmName + ".csv";
        FileUtils.touch(new File(fileName));
        FileUtils.forceDelete(new File(fileName));

        String csvHeader = String.format("%s,%s", "key size", "decryption time");
        Collection<String> csvData = new ArrayList<String>();
        csvData.add(csvHeader);

        List<PerformanceDataPoint> dataPoints = performanceStatsList.get(cryptoAlgorithmName).getAllPerformanceDataPoint();
        for (PerformanceDataPoint dataPoint : dataPoints) {
            String csvDataLines = String.format("%s,%S", dataPoint.getKeySize(), dataPoint.getDecryptionTime());
            csvData.add(csvDataLines);
        }
        FileUtils.writeLines(new File(fileName), csvData);
    }
    
    private void writeEncryptionAndDecryptionTimes(
            Map<String, IPerformanceStats> performanceStatsList, String cryptoAlgorithmName)
            throws IOException {
        String encryptionTimeFileName = "encryptionTime" + cryptoAlgorithmName + ".csv";
        String decryptionTimeFileName = "decryptionTime" + cryptoAlgorithmName + ".csv";

        FileUtils.touch(new File(encryptionTimeFileName));
        FileUtils.forceDelete(new File(encryptionTimeFileName));
        FileUtils.touch(new File(decryptionTimeFileName));
        FileUtils.forceDelete(new File(decryptionTimeFileName));

        Collection<String> encryptionCsvData = new ArrayList<String>();
        Collection<String> decryptionCsvData = new ArrayList<String>();

        List<PerformanceDataPoint> dataPoints = performanceStatsList.get(cryptoAlgorithmName).getAllPerformanceDataPoint();
        for (PerformanceDataPoint dataPoint : dataPoints) {
            String encryptionCsvDataLines = String.format("%s", dataPoint.getEncryptionTime());
            String decryptionCsvDataLines = String.format("%s", dataPoint.getDecryptionTime());
            encryptionCsvData.add(encryptionCsvDataLines);
            decryptionCsvData.add(decryptionCsvDataLines);
        }
        FileUtils.writeLines(new File(encryptionTimeFileName), encryptionCsvData);
        FileUtils.writeLines(new File(decryptionTimeFileName), decryptionCsvData);
    }

    private void writeKeyGenerationTimeCsv(
            Map<String, IPerformanceStats> performanceStatsList, String cryptoAlgorithmName)
            throws IOException {
        String keyGenTimeFileName = "keyGenTime" + cryptoAlgorithmName + ".csv";
        FileUtils.touch(new File(keyGenTimeFileName));
        FileUtils.forceDelete(new File(keyGenTimeFileName));

        Collection<String> keyGenTimeCsvData = new ArrayList<String>();
        List<PerformanceDataPoint> dataPoints = performanceStatsList.get(cryptoAlgorithmName).getAllPerformanceDataPoint();
        for (PerformanceDataPoint dataPoint : dataPoints) {
            String keyGenCsvDataLines = String.format("%s,%s", dataPoint.getKeySize(), dataPoint.getKeyGenerationTimeTaken());
            keyGenTimeCsvData.add(keyGenCsvDataLines);
        }
        FileUtils.writeLines(new File(keyGenTimeFileName), keyGenTimeCsvData);

    }
}
