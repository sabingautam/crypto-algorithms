package seis720.project.algorithms;

import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;

import seis720.project.entity.IPerformanceStats;
import seis720.project.entity.RSAPerformanceStats;
import seis720.project.service.PerformanceRecordingService;

/**
 * @author Sabin Gautam
 *
 */
public class RSA extends CryptoAlgorithm implements IAlgorithm {

    PerformanceRecordingService recordingService;

    public static final String ALGORITHM = "RSA";
    private final KeyPairGenerator keyGen;
    private final Cipher encryptCipher;
    private final Cipher decryptCipher;
    private IPerformanceStats rsaPerformanceStats;

    private PublicKey publicKey;
    private PrivateKey privateKey;

    public RSA(PerformanceRecordingService recordingService, RSAType rsaType) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {
        this.recordingService = recordingService;
        rsaPerformanceStats = new RSAPerformanceStats(rsaType);
        this.recordingService.registerNewIRecordingPerformanceStat(rsaPerformanceStats.getClass().getSimpleName(), rsaPerformanceStats);
        keyGen = KeyPairGenerator.getInstance(ALGORITHM);
        encryptCipher = Cipher.getInstance(rsaType.getKey());
        decryptCipher = Cipher.getInstance(rsaType.getKey());
        keySize = 1024;
        generateNewKey(keySize);
    }

    public void generateNewKey(int keySize) {
        Long startTime = System.currentTimeMillis();
        keyGen.initialize(keySize);
        this.keySize = keySize;
        KeyPair key = keyGen.generateKeyPair();
        publicKey = key.getPublic();
        privateKey = key.getPrivate();
        Long endTime = System.currentTimeMillis();
        keyGenerationTimeTaken = endTime - startTime;
    }

    public byte[] encrypt(String plainText) {
        enableRsaRecording();
        byte[] encryptedCipherTextBytes = {};
        byte[] plainTextBytes = plainText.getBytes();
        plainTextSize = plainTextBytes.length;

        try {
            encryptCipher.init(Cipher.ENCRYPT_MODE, publicKey);
            Long startTime = System.currentTimeMillis();
            encryptedCipherTextBytes = encryptCipher.doFinal(plainTextBytes);
            Long endTime = System.currentTimeMillis();
            encryptionTimeTaken = endTime - startTime;
        } catch (Exception e) {
          throw new RuntimeException("Error encrypting plaintext using public key", e);
        }
        return encryptedCipherTextBytes;
    }

    public byte[] decrypt(byte[] encryptedText) {
        enableRsaRecording();
        byte[] dectyptedText = null;
        try {
            decryptCipher.init(Cipher.DECRYPT_MODE, privateKey);
            Long startTime = System.currentTimeMillis();
            dectyptedText = decryptCipher.doFinal(encryptedText);
            Long endTime = System.currentTimeMillis();
            decryptionTimeTaken = endTime - startTime;
        } catch (Exception e) {
            throw new RuntimeException("Error encrypting plaintext using private key", e);
        }
        return dectyptedText;
    }

    private void enableRsaRecording() {
        recordingService.setCurrentRecordingPerformanceStat(rsaPerformanceStats);
    }

    public void recordPerformanceDataPoint() {
        generatePerformanceDataPoint();
        recordingService.addPerformanceDataPoint(dataPoint);
    }

    public enum RSAType {

        RSA_ECB("RSA/ECB/PKCS1Padding");

        private final String key;

        RSAType(String key) {
            this.key = key;
        }

        public String getKey() {
            return key;
        }
    }
}
