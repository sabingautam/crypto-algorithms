package seis720.project.algorithms;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

import seis720.project.entity.CBCDESPerformanceStats;
import seis720.project.entity.ECBDESPerformanceStats;
import seis720.project.entity.IPerformanceStats;
import seis720.project.service.PerformanceRecordingService;

/**
 * @author Sabin Gautam
 *
 */
public class DES extends CryptoAlgorithm implements IAlgorithm {

    public static final String ALGORITHM = "DES";

    private PerformanceRecordingService recordingService;
    private IPerformanceStats desPerformanceStats;
    private DESType desType;
    private final Cipher desCipher;
    private static SecretKey secretKey;
    private static final int KEY_SIZE = 56;

    public DES(PerformanceRecordingService recordingService, DESType desType) throws NoSuchAlgorithmException, NoSuchPaddingException, NoSuchProviderException {
        this.recordingService = recordingService;
        this.desType = desType;

        if (DESType.DES_CBC.equals(desType)) {
            desPerformanceStats = new CBCDESPerformanceStats();
        } else if (DESType.DES_ECB.equals(desType)) {
            desPerformanceStats = new ECBDESPerformanceStats();
        } else {
            throw new NoSuchAlgorithmException("Not a supported AES Algorithm");
        }

        this.recordingService.registerNewIRecordingPerformanceStat(desPerformanceStats.getClass().getSimpleName(), desPerformanceStats);
        keySize = KEY_SIZE;
        generateNewKey(KEY_SIZE);
        this.desCipher = Cipher.getInstance(desType.getKey());
        blockSize = desCipher.getBlockSize();
    }

    public byte[] encrypt(String plainText) {
        enableDESRecording();
        Long startTime = Long.MIN_VALUE;
        Long endTime = Long.MIN_VALUE;
        byte[] encryptedTextBytes = {};

        plainTextSize = plainText.getBytes().length;
        try {
            if (DESType.DES_CBC.equals(desType)) {
                final IvParameterSpec initVector = generateEncryptInitializationVector();
                desCipher.init(Cipher.ENCRYPT_MODE, secretKey, initVector);
                startTime = System.currentTimeMillis();
                encryptedTextBytes = desCipher.doFinal(plainText.getBytes());
                endTime = System.currentTimeMillis();
            } else if (DESType.DES_ECB.equals(desType)) {
                desCipher.init(Cipher.ENCRYPT_MODE, secretKey);
                startTime = System.currentTimeMillis();
                encryptedTextBytes =  desCipher.doFinal(plainText.getBytes());
                endTime = System.currentTimeMillis();
            } else {
                throw new NoSuchAlgorithmException("Not a supported DES Algorithm");
            }
        } catch (Exception e) {
            throw new RuntimeException("Error encrypting plain text", e);
        }
        encryptionTimeTaken = endTime - startTime;
        return encryptedTextBytes;
    }

    public byte[] decrypt(byte[] cipherText) {
        enableDESRecording();
        Long startTime = Long.MIN_VALUE;
        Long endTime = Long.MIN_VALUE;
        byte[] decryptedTextBytes = {};

        try {
            if (DESType.DES_CBC.equals(desType)) {
                byte [] iv = desCipher.getIV();
                final IvParameterSpec initVector = new IvParameterSpec(iv);
                desCipher.init(Cipher.DECRYPT_MODE, secretKey, initVector);
                startTime = System.currentTimeMillis();
                decryptedTextBytes = desCipher.doFinal(cipherText);
                endTime = System.currentTimeMillis();
            } else if (DESType.DES_ECB.equals(desType)) {
                desCipher.init(Cipher.DECRYPT_MODE, secretKey);
                startTime = System.currentTimeMillis();
                decryptedTextBytes =  desCipher.doFinal(cipherText);
                endTime = System.currentTimeMillis();
            } else {
                throw new NoSuchAlgorithmException("Not a supported DES Algorithm");
            }
        } catch (Exception e) {
            throw new RuntimeException("Error decrypting plain text", e);
        }
        decryptionTimeTaken = endTime - startTime;
        return decryptedTextBytes;
    }

    public void generateNewKey(int keySize) {
       if (keySize != KEY_SIZE) {
           System.out.println("Key size is not equal to " + KEY_SIZE + ". Using default key size KEY_SIZE");
       }
        try {
            Long startTime = System.currentTimeMillis();
            KeyGenerator keyGen = KeyGenerator.getInstance(ALGORITHM);
            secretKey = keyGen.generateKey();
            Long endTime = System.currentTimeMillis();
            keyGenerationTimeTaken = endTime - startTime;
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Error generating key", e);
        }
    }

    private void enableDESRecording() {
        recordingService.setCurrentRecordingPerformanceStat(desPerformanceStats);
    }

    public void recordPerformanceDataPoint() {
        generatePerformanceDataPoint();
        recordingService.addPerformanceDataPoint(dataPoint);
    }

    public enum DESType {
        DES_ECB("DES/ECB/PKCS5Padding"),
        DES_CBC("DES/CBC/PKCS5Padding");

        private final String key;

        DESType(String key) {
            this.key = key;
        }

        public String getKey() {
            return key;
        }
    }
}
