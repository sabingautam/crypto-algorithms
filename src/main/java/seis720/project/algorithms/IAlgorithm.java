package seis720.project.algorithms;


/**
 * @author Sabin Gautam
 *
 */
public interface IAlgorithm {

    byte[] encrypt(String plaintext);

    byte[] decrypt(byte[] cipherText);

    void generateNewKey(int keySize);

    void recordPerformanceDataPoint();
}
