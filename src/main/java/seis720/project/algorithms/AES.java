package seis720.project.algorithms;

import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import seis720.project.entity.CBCAESPerformanceStats;
import seis720.project.entity.ECBAESPerformanceStats;
import seis720.project.entity.IPerformanceStats;
import seis720.project.service.PerformanceRecordingService;

/**
 * 
 * @author Sabin Gautam
 *
 */
public class AES extends CryptoAlgorithm implements IAlgorithm {

    public static final String ALGORITHM = "AES";

    private PerformanceRecordingService recordingService;
    private IPerformanceStats aesPerformanceStats;
    private AESType aesType;
    private final Cipher encryptCipher;
    private final Cipher decryptCipher;
    private Key secretKey;

    public AES(PerformanceRecordingService recordingService, AESType aesType) throws NoSuchAlgorithmException, NoSuchPaddingException {
        this.recordingService = recordingService;
        this.aesType = aesType;
        random = new SecureRandom();

        if (AESType.AES_CBC.equals(aesType)) {
            aesPerformanceStats = new CBCAESPerformanceStats();
        } else if (AESType.AES_ECB.equals(aesType)) {
            aesPerformanceStats = new ECBAESPerformanceStats();
        } else {
            throw new NoSuchAlgorithmException("Not a supported AES Algorithm");
        }
        this.recordingService.registerNewIRecordingPerformanceStat(aesPerformanceStats.getClass().getSimpleName(), aesPerformanceStats);
        this.encryptCipher = Cipher.getInstance(aesType.getKey());
        this.decryptCipher = Cipher.getInstance(aesType.getKey());
        blockSize = decryptCipher.getBlockSize();
        keySize = 256;
        generateNewKey(keySize);
    }

    public void generateNewKey(int keySize) {
        try {
            Long startTime = System.currentTimeMillis();
            KeyGenerator keyGen = KeyGenerator.getInstance("AES");
            keyGen.init(keySize, new SecureRandom());
            SecretKey key = keyGen.generateKey();
            secretKey = new SecretKeySpec(key.getEncoded(), "AES");
            Long endTime = System.currentTimeMillis();
            keyGenerationTimeTaken = endTime - startTime;
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Error!!! No Such Algorithm", e);
        }
    }

    public byte[] encrypt(String plainText) {
        enableAesRecording();

        Long startTime = Long.MIN_VALUE;
        Long endTime = Long.MIN_VALUE;
        byte [] plainTextBytes = plainText.getBytes();
        plainTextSize = plainTextBytes.length;
        byte[] encryptedTextBytes = {};

        try {
            if (AESType.AES_CBC.equals(aesType)) {
                IvParameterSpec iv = generateEncryptInitializationVector();
                encryptCipher.init(Cipher.ENCRYPT_MODE, secretKey, iv);
                int outputSize = encryptCipher.getOutputSize(plainTextBytes.length);
                encryptedTextBytes = new byte[blockSize + outputSize];
                System.arraycopy(iv.getIV(), 0, encryptedTextBytes, 0, blockSize);
                startTime = System.currentTimeMillis();
                encryptCipher.doFinal(plainTextBytes, 0, plainTextBytes.length, encryptedTextBytes, blockSize);
                endTime = System.currentTimeMillis();
            } else if (AESType.AES_ECB.equals(aesType)) { 
                encryptCipher.init(Cipher.ENCRYPT_MODE, secretKey);
                startTime = System.currentTimeMillis();
                encryptedTextBytes = encryptCipher.doFinal(plainTextBytes);
                endTime = System.currentTimeMillis();
            } else {
                throw new NoSuchAlgorithmException("Not a supported AES Algorithm");
            }
            encryptionTimeTaken = endTime - startTime;
        } catch (Exception e) {
            throw new RuntimeException("Error encrypting plain text", e);
        }
        return encryptedTextBytes;
    }

    public byte[] decrypt(byte[] cipherText) {
        enableAesRecording();
        byte [] plainTextBytes = {};
        Long startTime = Long.MIN_VALUE;
        Long endTime = Long.MIN_VALUE;
        try {
            if (AESType.AES_CBC.equals(aesType)) {
                IvParameterSpec iv = generateDecryptInitializationVector(cipherText);
                decryptCipher.init(Cipher.DECRYPT_MODE, secretKey, iv);
                startTime = System.currentTimeMillis();
                plainTextBytes = decryptCipher.doFinal(cipherText, blockSize, cipherText.length - blockSize);
                endTime = System.currentTimeMillis();
            } else if (AESType.AES_ECB.equals(aesType)) { 
                decryptCipher.init(Cipher.DECRYPT_MODE, secretKey);
                startTime = System.currentTimeMillis();
                plainTextBytes = decryptCipher.doFinal(cipherText, 0, cipherText.length);
                endTime = System.currentTimeMillis();
            } else {
                throw new NoSuchAlgorithmException("Not a supported AES Algorithm");
            }
            decryptionTimeTaken = endTime - startTime;
        } catch (Exception e) {
            throw new RuntimeException("Error decrypting plain text", e);
        }
        return plainTextBytes;
    }

    private void enableAesRecording() {
        recordingService.setCurrentRecordingPerformanceStat(aesPerformanceStats);
    }

    public void recordPerformanceDataPoint() {
        generatePerformanceDataPoint();
        recordingService.addPerformanceDataPoint(dataPoint);
    }

    public enum AESType {
        AES_ECB("AES/ECB/PKCS5Padding"),
        AES_CBC("AES/CBC/PKCS5Padding");

        private final String key;

        AESType(String key) {
            this.key = key;
        }

        public String getKey() {
            return key;
        }
    }

}
