package seis720.project.algorithms;

import java.security.Key;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import seis720.project.entity.CBCTrippleDESPerformanceStats;
import seis720.project.entity.ECBTrippleDESPerformanceStats;
import seis720.project.entity.IPerformanceStats;
import seis720.project.service.PerformanceRecordingService;

/**
 * @author Sabin Gautam
 *
 */
public class TrippleDES extends CryptoAlgorithm implements IAlgorithm {

    public static final String ALGORITHM = "DESede";

    private PerformanceRecordingService recordingService;
    private IPerformanceStats des3PerformanceStats;
    private TrippleDESType des3Type;
    private final Cipher encryptCipher;
    private final Cipher decryptCipher;
    private Key secretKey;
    private final IvParameterSpec initVector;

    public TrippleDES(PerformanceRecordingService recordingService, TrippleDESType des3Type) throws NoSuchAlgorithmException, NoSuchPaddingException {
        this.recordingService = recordingService;
        this.des3Type = des3Type;
        keySize = 112;
        if (TrippleDESType.DES_3_CBC.equals(des3Type)) {
            des3PerformanceStats = new CBCTrippleDESPerformanceStats();
        } else if (TrippleDESType.DES_3_ECB.equals(des3Type)) {
            des3PerformanceStats = new ECBTrippleDESPerformanceStats();
        } else {
            throw new NoSuchAlgorithmException("Not a supported AES Algorithm");
        }

        this.recordingService.registerNewIRecordingPerformanceStat(des3PerformanceStats.getClass().getSimpleName(), des3PerformanceStats);
        generateNewKey(keySize);
        this.encryptCipher = Cipher.getInstance(des3Type.getKey());
        this.decryptCipher = Cipher.getInstance(des3Type.getKey());
        blockSize = decryptCipher.getBlockSize();
        initVector = new IvParameterSpec(new byte[8]);
    }

    public byte[] encrypt(String plainText) {
        enableTrippleDESRecording();

        Long startTime = Long.MIN_VALUE;
        Long endTime = Long.MIN_VALUE;

        byte[] encryptedTextBytes = {};
        byte[] plainTextBytes = plainText.getBytes();
        plainTextSize = plainTextBytes.length;
        try {
            if (TrippleDESType.DES_3_CBC.equals(des3Type)) {
                encryptCipher.init(Cipher.ENCRYPT_MODE, secretKey, initVector);
                startTime = System.currentTimeMillis();
                encryptedTextBytes = encryptCipher.doFinal(plainTextBytes);
                endTime = System.currentTimeMillis();
            } else if (TrippleDESType.DES_3_ECB.equals(des3Type)) {
                encryptCipher.init(Cipher.ENCRYPT_MODE, secretKey);
                startTime = System.currentTimeMillis();
                encryptedTextBytes =  encryptCipher.doFinal(plainText.getBytes());
                endTime = System.currentTimeMillis();
            } else {
                throw new NoSuchAlgorithmException("Not a supported AES Algorithm");
            }
        } catch (Exception e) {
            throw new RuntimeException("Error encrypting plain text", e);
        }
        encryptionTimeTaken = endTime - startTime;
        return encryptedTextBytes;
    }

    public byte[] decrypt(byte[] cipherTextBytes) {
        enableTrippleDESRecording();

        Long startTime = Long.MIN_VALUE;
        Long endTime = Long.MIN_VALUE;

        byte[] encryptedTextBytes = {};
        try {
            if (TrippleDESType.DES_3_CBC.equals(des3Type)) {
                decryptCipher.init(Cipher.DECRYPT_MODE, secretKey, initVector);
                startTime = System.currentTimeMillis();
                encryptedTextBytes = decryptCipher.doFinal(cipherTextBytes);
                endTime = System.currentTimeMillis();
            } else if (TrippleDESType.DES_3_ECB.equals(des3Type)) {
                decryptCipher.init(Cipher.DECRYPT_MODE, secretKey);
                startTime = System.currentTimeMillis();
                encryptedTextBytes =  decryptCipher.doFinal(cipherTextBytes);
                endTime = System.currentTimeMillis();
            } else {
                throw new NoSuchAlgorithmException("Not a supported AES Algorithm");
            }
        } catch (Exception e) {
            throw new RuntimeException("Error decrypting plain text", e);
        }
        decryptionTimeTaken = endTime - startTime;
        return encryptedTextBytes;
    }

    public void generateNewKey(int keySize) {
        try {
        	Long startTime = System.currentTimeMillis();
            KeyGenerator keyGen = KeyGenerator.getInstance(ALGORITHM);
            keyGen.init(keySize);
            Key key = keyGen.generateKey();
            secretKey = new SecretKeySpec(key.getEncoded(), 0, key.getEncoded().length, ALGORITHM);
            Long endtime = System.currentTimeMillis();
            keyGenerationTimeTaken = endtime - startTime;
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Error generating key", e);
        }
    }

    private void enableTrippleDESRecording() {
        recordingService.setCurrentRecordingPerformanceStat(des3PerformanceStats);
    }

    public void recordPerformanceDataPoint() {
        generatePerformanceDataPoint();
        recordingService.addPerformanceDataPoint(dataPoint);
    }

    public enum TrippleDESType {
        DES_3_ECB("DESede/ECB/PKCS5Padding"),
        DES_3_CBC("DESede/CBC/PKCS5Padding");

        private final String key;

        TrippleDESType(String key) {
            this.key = key;
        }

        public String getKey() {
            return key;
        }
    }
}
