package seis720.project.algorithms;

import java.security.SecureRandom;
import java.util.Arrays;

import javax.crypto.spec.IvParameterSpec;

import seis720.project.entity.PerformanceDataPoint;

/**
 * 
 * @author Sabin Gautam
 *
 */
public abstract class CryptoAlgorithm {

    protected int blockSize = 8; //default block size
    protected SecureRandom random = new SecureRandom();
    protected int plainTextSize = 0;
    protected Long encryptionTimeTaken = Long.MIN_VALUE;
    protected Long decryptionTimeTaken = Long.MIN_VALUE;
    protected Long keyGenerationTimeTaken = Long.MIN_VALUE;
    protected int keySize = 0;
    protected PerformanceDataPoint dataPoint;

    protected IvParameterSpec generateEncryptInitializationVector() {
        byte[] initVector = new byte[blockSize];
        random.nextBytes(initVector);
        return new IvParameterSpec(initVector);
    }

    protected IvParameterSpec generateDecryptInitializationVector(byte[] input) {
        byte[] initVector = Arrays.copyOf(input, blockSize);
        return new IvParameterSpec(initVector);
    }

    protected void generatePerformanceDataPoint() {
        dataPoint = new PerformanceDataPoint(encryptionTimeTaken,
                                             decryptionTimeTaken,
                                             keyGenerationTimeTaken,
                                             blockSize,
                                             keySize,
                                             plainTextSize);
    }
}
