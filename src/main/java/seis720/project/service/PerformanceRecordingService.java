package seis720.project.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import seis720.project.entity.IPerformanceStats;
import seis720.project.entity.PerformanceDataPoint;

/**
 * @author Sabin Gautam
 *
 */
@Service("recordingService")
public class PerformanceRecordingService {

    private IPerformanceStats currentRecordingPerformanceStat;
    private Map<String, IPerformanceStats> performanceStatsList = new HashMap<String, IPerformanceStats>();

    public void addPerformanceDataPoint(PerformanceDataPoint dataPoint) {
        currentRecordingPerformanceStat.addPerformanceDataPoint(dataPoint);
    }

    public Map<String, IPerformanceStats> getAllPerformanceStats() {
        return performanceStatsList;
    }

    public void setCurrentRecordingPerformanceStat (IPerformanceStats iPerformanceStats) {
        this.currentRecordingPerformanceStat = iPerformanceStats;
    }

    public void registerNewIRecordingPerformanceStat(String name, IPerformanceStats iPerformanceStats) {
        performanceStatsList.put(name, iPerformanceStats);
    }
}
